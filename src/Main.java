/**
 * Created by susinths on 10/18/13.
 */
import java.util.*;

public class Main {
    static int maxnr = 0, totTime = 0;

    public static void main(String[] args) {
        Map<Integer, Task> map=new TreeMap<>();
        //Scanner scanner = new Scanner(Main.class.getResourceAsStream("/res/buildhouse1.txt"), "UTF-8");
        String filename = args[0];
        Scanner scanner = new Scanner(Main.class.getResourceAsStream(filename), "UTF-8");
        Main mainObject = new Main();
        map = mainObject.fromScanner(scanner);
        //System.out.println("DEBUG: maxnr " + maxnr);
        mainObject.setInDegrees(map);
        mainObject.setOutEdges(map);
        //System.out.println("DEBUG Main: indegree for Task number 8 is " + map.get(8).getIndegrees());
        ArrayList<Task> topSortList = mainObject.topSort(maxnr, map);
        mainObject.setInDegrees(map); // since topSort(maxnr, map) decreases the indegrees of the nodes
        mainObject.setEarliestStart(topSortList);
        mainObject.runProject(topSortList);
        mainObject.setLatestStart(topSortList,map);
        Collections.reverse(topSortList); // since setLatestStart reverses topSortList
        mainObject.printTasksInfo(map);

        //System.out.println("DEBUG 2 Main: earliestStart for Task number 6 is " + topSortList.get(2).getEarliestStart());
        //System.out.println("DEBUG Main: check for sorted Map first Task in map" + map.get());

    } // END of void main(String[] args)

    public void setInDegrees(Map<Integer, Task> map) {
        for(Integer k:map.keySet()) {
            Task task = map.get(k);
            task.setIndegree();
        }
    }


    public void setOutEdges(Map<Integer, Task> map) {
        for(Integer k:map.keySet()) {
            // map.get(k).setIndegrees();
            for(Integer dep:map.get(k).getDependencies()) {
                //System.out.println("DEBUG: "+ dep);
                //System.out.print(" DEBUG:  Name is "  + map.get(dep).getName());
                map.get(dep).setOutEdge(map.get(k));
            }
            //System.out.println(map.get(k));
        }
    } //END of setOutEdges()

    public ArrayList<Task> topSort(int maxnr, Map<Integer, Task> map) {
        ArrayList<Task> L = new ArrayList<Task>(maxnr);
        //LinkedList<Task> L = new LinkedList<>();
        //HashSet<Task> S = new HashSet<>(maxnr);
        LinkedList<Task> S = new LinkedList<>();

        for(Integer n:map.keySet()) {
            if(map.get(n).getIndegrees() == 0) {
                S.add(map.get(n));
            }
        }
        Task t;
        int counter= 0;
        while(!S.isEmpty()) {
            //System.out.print("Topsort: Task and S. " + t.getNumber());
            t = S.iterator().next();
            S.remove(t);
            //System.out.print("Topsort : " + t.getNumber());
            L.add(t);
            //System.out.println("Starting " + t.getNumber());
            counter++;

            for(Iterator<Task> it = t.getOutEdges().iterator(); it.hasNext();) {
                Task w =  it.next();

                w.cntPredecessors--;
                if (w.getIndegrees() == 0) {
                    S.add(w);
                   // System.out.println("Starting " + w.getNumber());
                }
            }
        }
        System.out.println();

        if (counter < maxnr) {
            System.out.println("Cycle detected, topsort not possible");
            System.exit(0);
        } else {
            //System.out.println("Topsort : " + Arrays.toString(L.toArray()));
            Iterator<Task> topsortIt = L.iterator();
            System.out.print("\n Topsort list is: ");
            while (topsortIt.hasNext()) {
                System.out.print(" " + topsortIt.next().getNumber());

            }
            System.out.println();
        }
        return L;
    } //END of topSort()

    public Map fromScanner(Scanner scanner) {
    Map<Integer, Task> map=new HashMap<>();
    maxnr = scanner.nextInt();
    while (scanner.hasNextLine()) {
        String line=scanner.nextLine();
        if (line.isEmpty() ) continue;
        Scanner s2=new Scanner(line);
        Task task = new Task(s2.nextInt(), s2.next(), s2.nextInt(), s2.nextInt());
        while (s2.hasNextInt()) {
            int i = s2.nextInt();
            if (i != 0) {
                task.getDependencies().add(i);
            }
        }
        map.put(task.getNumber(), task);
    }
    return map;
    } //END of fromScanner()

    public void printTasksInfo( Map<Integer, Task> map) {
        System.out.println("Critical tasks has slack=0 in the listing below");
        System.out.println("No.\tTime\tStaff\tSlack\tLatestStart\t Deps [No.] \tName" );
        for(int i: map.keySet()) {
            Task task = map.get(i);
          System.out.println(task.getNumber() + "\t" + task.getTime() + "    \t" +task.getStaff()+ "    \t" + task.getSlack() +"    \t" +  task.getLatestStart() + "         \t" + task.getOutEdgesNumber() +"    \t\t" + task.getName() );
        }
   }

    public void setEarliestStart(ArrayList<Task> topSortList) {
        Iterator<Task> taskIterator = topSortList.iterator();
        Task task;
        int totTime = 0;
        //System.out.println(" DEBUG: setEarliestStart:  Time:  " + 0 + " Starting: " + 5 );
        while(taskIterator.hasNext()) {
                task = taskIterator.next();
                int taskTime = task.getEarliestStart() + task.getTime();
                totTime = taskTime;
                //System.out.println(" DEBUG: setEarliestStart:  Time:  " + taskTime + " Finished: " + task.getNumber());

                if (task.getIndegrees() == 0) {
                    //System.out.println("DEBUG: setEarliestStart:  setting earliestStart = 0 for " + task.getNumber());
                    task.setEarliestStart(0);
                }
                if (task.getOutEdges() != null) {
                    Iterator<Task> outEdgeIterator = task.getOutEdges().iterator();
                    while(outEdgeIterator.hasNext()) {
                        Task edgeTask = outEdgeIterator.next();
                        if ( (task.getEarliestStart() + task.getTime()) > edgeTask.getEarliestStart()) {
                            edgeTask.setEarliestStart(task.getEarliestStart() + task.getTime());
                         }
                    }
                }
            //System.out.println();
            }
        //System.out.println("\n****Shortest possible project execution time is " +totTime + " ****");
    }

    public void runProject(ArrayList<Task> topSortList) {
        //System.out.println(" DEBUG: runProject:  topList is  "  + topSortList );
        Iterator<Task> taskIterator = topSortList.iterator();
        Task task;
        int staff= 0;
        //System.out.println(" DEBUG: runProject:  Time:  " + 0 + " Starting: " + 5 );
        while(taskIterator.hasNext()) {
            task = taskIterator.next();
            if (task.getIndegrees() == 0) {
                //System.out.println("DEBUG: setEarliestStart:  setting earliestStart = 0 for " + task.getNumber());
                System.out.println("Time:\t" + task.getEarliestStart() + "\tStarting: " + task.getNumber() );
                staff = task.getStaff();
                System.out.println("\t\tCurrent staff: " + staff);
            }
            int taskTime = task.getEarliestStart() + task.getTime();
            totTime = taskTime;
            System.out.println("Time:\t" + taskTime + "\tFinished: " + task.getNumber());
            staff -= task.getStaff();


            if (task.getOutEdges() != null) {
                Iterator<Task> outEdgeIterator = task.getOutEdges().iterator();
                while(outEdgeIterator.hasNext()) {
                    Task edgeTask = outEdgeIterator.next();

                        if( edgeTask.getEarliestStart() == taskTime) {
                        System.out.println("\t\t\t Starting: " + edgeTask.getNumber() );
                            staff += edgeTask.getStaff();

                        }
                }
                System.out.println("\t\tCurrent staff: " + staff);
                //go through  topList and do the printing!
            }
        }
        System.out.println("\n****Shortest possible project execution time is " +totTime + " ****");
        System.out.println();
    }

    public void setLatestStart (ArrayList<Task> topSortList, Map<Integer, Task> map) {
        Collections.reverse(topSortList);
        int completionTime = totTime; // will be changed to completion time returned by setEarliestStart
        //System.out.println("DEBUG: setLatestStart: reversed array is: " + topSortList);
        Iterator<Task> taskIterator = topSortList.iterator();
        while(taskIterator.hasNext()) {
           Task task = taskIterator.next();
            //System.out.println("DEBUG: setLatestStart: Task reversed array is: " + task.getNumber() +  " has latestStart " + task.getLatestStart());
                if (task.getOutEdges().size() == 0) {
                    task.setLatestStart(completionTime - task.getTime());
                    //System.out.println("DEBUG: setLatestStart:  no outEdges: latestStart for " +task.getNumber() +  " is " +  task.getLatestStart());
                }
                    //} else {
                            Iterator<Integer> depsIterator = task.getDependencies().iterator(); //Problem: Task.dependencies is a list of integers!
                            while(depsIterator.hasNext()){
                                    Task nextDep = map.get(depsIterator.next());
                                //System.out.println("DEBUG: setLatestStart:  Task from map Task " + nextDep.getNumber() + " and has latestStart : " + nextDep.getLatestStart());
                                    if ( (task.getLatestStart() - nextDep.getTime()) < nextDep.getLatestStart()) {
                                            nextDep.setLatestStart(task.getLatestStart() - nextDep.getTime() );
                            }
                            }
                }


        }


} //END of class Main










