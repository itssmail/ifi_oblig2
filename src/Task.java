/**
* Created by susinths on 10/18/13.
*/
import java.util.ArrayList;
import java.util.List;
import java.util.*;

public class Task {
    int number;
    String name;
    int time;
    int staff;
    int cost;
    int earliestStart, latestStart;
    List<Integer> dependencies;
    List<Task> outEdges;
    int cntPredecessors;
    Status status;
    public enum Status {UNVISITED,RUNNING,VISITED};
    @Override
    public String toString() {
        return "Task{" +
                "number=" + number +
                ", name='" + name + '\'' +
                ", time=" + time +
                ", staff=" + staff +
                ", dependencies=" + dependencies +
                '}';
    }

    public Task(int number, String name, int time, int staff) {
        setNumber(number);
        setName(name);
        setTime(time);
        setStaff(staff);
        earliestStart = 0;
        latestStart = Integer.MAX_VALUE;
        dependencies=new ArrayList<>();
        outEdges=new ArrayList<>();
        status = Status.UNVISITED;
        setCost(time);
     }

    public int getSlack() {
        return this.latestStart - this.earliestStart;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public void setEarliestStart(int earliestTime) { this.earliestStart = earliestTime;}

    public int getEarliestStart() { return this.earliestStart;}

    public void setLatestStart(int latestTime) { this.latestStart = latestTime;}

    public int getLatestStart() { return this.latestStart;}


    public int getStaff() {
        return staff;
    }

    public void setStaff(int staff) {
        this.staff = staff;
    }

    public List<Integer> getDependencies() {
        return dependencies;
    }

    public void setDependencies(List<Integer> dependencies) {
        this.dependencies = dependencies;
    }

    public List<Task> getOutEdges() {return outEdges; }

    public List<Integer> getOutEdgesNumber() {
    List<Integer> outEdgesDep = new ArrayList<Integer>();
    Iterator<Task> outEdgeIterator = this.outEdges.iterator();
        while(outEdgeIterator.hasNext()) {
            outEdgesDep.add(outEdgeIterator.next().getNumber());
        }
        return outEdgesDep;
    }


    public void setOutEdge(Task t) {outEdges.add(t); }


    public int getIndegrees() { return cntPredecessors; }

    public void setIndegree() { cntPredecessors = dependencies.size();}

    public Status getStatus() {return this.status; }

    public Task findTaskWithNoInDegree() {
            if (this.cntPredecessors == 0) return this;
            return null;
    }



/*
    public int compareTo(Task other) {

        return Double.compare(this.time, other.time);

        }*/
}


